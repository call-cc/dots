#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
# PS1="[\u@\h \W]\$ " 
alias l="ls --color=auto"
export CVSROOT="$HOME/dev/cvs"
export GO111MODULE=on
export GOPATH="$HOME/golib"
export PATH="$HOME/.cabal/bin:$HOME/.ghcup/bin:$PATH:$HOME/.local/bin:/usr/local/go/bin:$GOPATH/bin"
export GOPATH="$GOPATH:$HOME/dev/go/"
PS1="[\W] "

